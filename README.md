# Payday Trade
- Application is scalable, since microservice architecture is used.
- ApiGateway balances the load on microservices. When required a new service instance can be run and register itself on Eureka Discovery Server (PaydayDiscoveryService project).

## Access To Services
- Anoyone can access **Sign Up** or **Sign In** services.
- Other services are authenticated using JWT. Routing configuration can be found in **application.properties** file of ApiGateway project (Spring Cloud API Gateway).

## Isoltion of Environments
- A copy application.properties file is created in each project with name **application-production.properties**.
- Spring Boot applications will be deployed as Docker containers.
- When starting the docker image SPRING_PROFILES_ACTIVE environemnt variable should be set to specify which properties file will be used. If SPRING_PROFILES_ACTIVE=production, then **application-production.properties** will be used.
- Properties files should be moved to a centralized config server (Spring Cloud Config Server) to reduce maintenance workload. On centralized config server, application specific config files can be stored with applcation-name.properties (customers-ws.properties for deafult profile, customers-ws-**production**.properties for production profile).
- If required different beans can be autowired depending on the container's active profile (with @Profile annotation).

## 3rd Party Brokerage ApiGateway
- We know external system is unreliable, so that HTTP Client (FeignClient) is configured with NEVER_RETRY policy.
- Still we need to store last order in database to check transaction consistencty later and notify the customer accordingly.
- If 3rd party service provider supports idempotency, FeignClient can be configured with exponential backoff strategy.

## Application Docker Images
- Docker container images are created for each application module and pushed into following repository.
	- https://hub.docker.com/u/turac
- These images can be used to create Kubernetes pods for deployment.

## Postman for locale testing
- postman_payday_collection.json file can be imported to test REST services locally.
	- https://gitlab.com/softtech-cenk/payday