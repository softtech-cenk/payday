package com.ibar.payday.discovery;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class PaydayDiscoveryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PaydayDiscoveryServiceApplication.class, args);
	}
	
}
