package com.ibar.payday.api.customers.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="customers")
public class CustomerEntity implements Serializable {

	private static final long serialVersionUID = -1882607684621309315L;
	
	@Id
	@GeneratedValue
	private long id;
	
	@Column(nullable=false, length=100, unique=true)
	private String customerId;
	
	@Column(nullable=false, length=50, unique=true)
	private String userName;
	
	@Column(nullable=false, length=100, unique=true)
	private String email;
	
	@Column(nullable=false)
	private String passwordEncrypted;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPasswordEncrypted() {
		return passwordEncrypted;
	}

	public void setPasswordEncrypted(String passwordEncrypted) {
		this.passwordEncrypted = passwordEncrypted;
	}

}
