package com.ibar.payday.api.customers.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.ibar.payday.api.customers.dto.CustomerDTO;

public interface CustomersService extends UserDetailsService {
	CustomerDTO createCustomer(CustomerDTO customerInfo);
	CustomerDTO getCustomerByUserName(String userName);
}
