package com.ibar.payday.api.customers.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CreateCustomerRequestModel {
	
	@NotNull(message="userName is a required field")
	private String userName;
	
	@Email(message="not a vali email")
	@NotNull(message="email is a required field")
	private String email;
	
	@NotNull(message="password is a required field")
	@Size(min=6, message="password should be at least 6 characters")
	@Pattern(regexp="^[a-zA-Z0-9]*",message="only alphanumeric characters are allowed") 
	private String password;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

}
