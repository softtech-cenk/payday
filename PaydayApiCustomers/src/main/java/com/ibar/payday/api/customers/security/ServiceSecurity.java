package com.ibar.payday.api.customers.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.ibar.payday.api.customers.service.CustomersService;

@Configuration
@EnableWebSecurity
public class ServiceSecurity extends WebSecurityConfigurerAdapter{
	
	private Environment env;
	private CustomersService customersService;
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	public ServiceSecurity(Environment env, CustomersService customersService, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.env = env;
		this.customersService = customersService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		
		// csrf disabled since service runs at backend 
		http.csrf().disable();
		
		// allow requests from API gateway
		http.authorizeRequests()
			.antMatchers("/**")
			.hasIpAddress(env.getProperty("api.gateway.ip"))
			.and()
			.addFilter(getAuthenticationFilter());
	}
	
	private CustomerAuthenticationFilter getAuthenticationFilter() throws Exception
	{
		CustomerAuthenticationFilter authenticationFilter = new CustomerAuthenticationFilter(customersService, env, authenticationManager()); 
		authenticationFilter.setFilterProcessesUrl(env.getProperty("signin.url"));
		return authenticationFilter;
	}
	
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(customersService).passwordEncoder(bCryptPasswordEncoder);
    }
}
