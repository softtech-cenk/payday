package com.ibar.payday.api.customers.repository;

import org.springframework.data.repository.CrudRepository;

import com.ibar.payday.api.customers.entity.CustomerEntity;

public interface CustomerRepository extends CrudRepository<CustomerEntity, Long> {
	CustomerEntity findByUserName(String userName);
}
