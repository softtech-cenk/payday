package com.ibar.payday.api.customers.service.impl;

import java.util.ArrayList;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.ibar.payday.api.customers.dto.CustomerDTO;
import com.ibar.payday.api.customers.entity.CustomerEntity;
import com.ibar.payday.api.customers.repository.CustomerRepository;
import com.ibar.payday.api.customers.service.CustomersService;

@Service
public class CustomersServiceImpl implements CustomersService {

	CustomerRepository repository;
	BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Autowired
	public CustomersServiceImpl(CustomerRepository repository, BCryptPasswordEncoder bCryptPasswordEncoder) {
		this.repository = repository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
	}
	
	@Override
	public CustomerDTO createCustomer(CustomerDTO customerInfo) {
		// Create Unique Id for customer that can be used on the client side
		customerInfo.setCustomerId(UUID.randomUUID().toString());
		customerInfo.setPasswordEncrypted(bCryptPasswordEncoder.encode(customerInfo.getPassword()));
		
		ModelMapper modelMapper = new ModelMapper();
		// source and destination property names should match strictly
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		CustomerEntity customerEntity = modelMapper.map(customerInfo, CustomerEntity.class);
		repository.save(customerEntity);
		CustomerDTO response = modelMapper.map(customerEntity, CustomerDTO.class);
		return response;
	}

  @Override
  public CustomerDTO getCustomerByUserName(String userName) {
    CustomerEntity customerEntity = repository.findByUserName(userName);

    if(customerEntity == null) {
      throw new UsernameNotFoundException(userName);
    }

    return new ModelMapper().map(customerEntity, CustomerDTO.class);
  }

  @Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		CustomerEntity customerEntity = repository.findByUserName(username);
		
		if (customerEntity == null) {
			throw new UsernameNotFoundException(username);
		}

		return new User(
		        customerEntity.getUserName(),
            customerEntity.getPasswordEncrypted(),
            true,
            true,
            true,
            true,
            new ArrayList<>()
    );
	}

}
