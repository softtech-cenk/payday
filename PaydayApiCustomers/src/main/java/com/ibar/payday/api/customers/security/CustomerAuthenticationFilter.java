package com.ibar.payday.api.customers.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ibar.payday.api.customers.dto.CustomerDTO;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ibar.payday.api.customers.model.SigninRequestModel;
import com.ibar.payday.api.customers.service.CustomersService;

public class CustomerAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private CustomersService customersService;
	private Environment environment;
	
	public CustomerAuthenticationFilter(CustomersService customersService, 
			Environment environment, 
			AuthenticationManager authenticationManager) {
		this.customersService = customersService;
		this.environment = environment;
		super.setAuthenticationManager(authenticationManager);
	}
	
    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
  
            SigninRequestModel creds = new ObjectMapper()
                    .readValue(req.getInputStream(), SigninRequestModel.class);
            
            return getAuthenticationManager().authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUserName(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
            
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {
    	String userName = ((User) auth.getPrincipal()).getUsername();
      CustomerDTO customerInfo = customersService.getCustomerByUserName(userName);
    	
        String token = Jwts.builder()
                .setSubject(customerInfo.getCustomerId())
                .setExpiration(
                        new Date(
                                System.currentTimeMillis()
                                + Long.parseLong(environment.getProperty("jwt_expiration"))
                        )
                )
                .signWith(SignatureAlgorithm.HS512, environment.getProperty("jwt_key") )
                .compact();
        
        res.addHeader("token", token);
        res.addHeader("userId", customerInfo.getCustomerId());
    }
}
