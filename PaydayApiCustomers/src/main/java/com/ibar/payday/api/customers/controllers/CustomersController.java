package com.ibar.payday.api.customers.controllers;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibar.payday.api.customers.dto.CustomerDTO;
import com.ibar.payday.api.customers.model.CreateCustomerRequestModel;
import com.ibar.payday.api.customers.model.CreateCustomerResponseModel;
import com.ibar.payday.api.customers.service.CustomersService;

@RestController
@RequestMapping("/customers")
public class CustomersController {
	
	@Autowired
	CustomersService customersService;
	
	@GetMapping("/healthcheck")
	public String healthCheck() {
		return "Success...";
	}

	@PostMapping
	public ResponseEntity<CreateCustomerResponseModel> signUp(@Valid @RequestBody CreateCustomerRequestModel customerInfo) {
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		CustomerDTO customerDTO = modelMapper.map(customerInfo, CustomerDTO.class); 
		CustomerDTO responseDTO = customersService.createCustomer(customerDTO);
		CreateCustomerResponseModel responseModel = modelMapper.map(responseDTO, CreateCustomerResponseModel.class);
		return ResponseEntity.status(HttpStatus.CREATED).body(responseModel);
	}
}
