package com.ibar.payday.api.customers;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.ibar.payday.api.customers.controllers.CustomersController;

@SpringBootTest
class PaydayApiCustomersApplicationTests {

	@Autowired
	CustomersController controller;
	
	@Test
	void contextLoads() {
		assertThat(controller).isNotNull();
	}

}
