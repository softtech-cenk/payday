package com.ibar.payday.api.stocks.repository;

import com.ibar.payday.api.stocks.dto.OrderDTO;

public interface OrdersRepository {

	void insertOrderDetails(OrderDTO orderDTO);
	
	void updateOrderDetails(OrderDTO orderDTO);
}
