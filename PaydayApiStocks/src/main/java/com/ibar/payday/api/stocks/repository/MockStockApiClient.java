package com.ibar.payday.api.stocks.repository;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.ibar.payday.api.stocks.model.MockOrderStatusResponseModel;
import com.ibar.payday.api.stocks.model.MockOrderStockRequestModel;
import com.ibar.payday.api.stocks.model.MockOrderStockResponseModel;
import com.ibar.payday.api.stocks.model.MockStockResponseModel;

@FeignClient(name="mockstocks-ws", configuration = CustomFeignConfiguration.class)
public interface MockStockApiClient {

	@GetMapping("/mockstocks")
	public List<MockStockResponseModel> getStocks();
	
	@PostMapping("/mockstocks/order")
	public MockOrderStockResponseModel order(@RequestBody MockOrderStockRequestModel orderDeatils);
	
	@GetMapping("/mockstocks/order/{orderId}")
	public List<MockOrderStatusResponseModel> getOrderStatus(@PathVariable String orderId);
}
