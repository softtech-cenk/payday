package com.ibar.payday.api.stocks.controllers;

import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ibar.payday.api.stocks.dto.OrderDTO;
import com.ibar.payday.api.stocks.dto.StockDTO;
import com.ibar.payday.api.stocks.model.OrderStockRequestModel;
import com.ibar.payday.api.stocks.model.OrderStockResponseModel;
import com.ibar.payday.api.stocks.model.StockResponseModel;
import com.ibar.payday.api.stocks.service.StocksService;

@RestController
@RequestMapping("/stocks")
public class StocksController {

	@Autowired
	StocksService stocksService;
	
	@GetMapping("/healthcheck")
	public String healthCheck() {
		return "Success...";
	}
	
	@GetMapping
	public List<StockResponseModel> listStocks() {
		List<StockDTO> stocks = stocksService.listStocks();
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		List<StockResponseModel> listStocks = new ArrayList<StockResponseModel>();
		for (StockDTO stock : stocks) {
			listStocks.add(modelMapper.map(stock, StockResponseModel.class));
		}
		return listStocks;
	}
	
	@PostMapping("/order")
	public ResponseEntity<OrderStockResponseModel> order(@RequestBody OrderStockRequestModel orderDeatils) {
		OrderStockResponseModel responseModel = new OrderStockResponseModel();
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		OrderDTO orderDTO = modelMapper.map(orderDeatils, OrderDTO.class);
		OrderDTO responseDTO = stocksService.placeOder(orderDTO);
		responseModel = modelMapper.map(responseDTO, OrderStockResponseModel.class);
		return ResponseEntity.status(HttpStatus.CREATED).body(responseModel);
	}
}
