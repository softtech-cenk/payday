package com.ibar.payday.api.stocks.dto;

import java.math.BigDecimal;

public class OrderDTO {
	private long depositAccountId;
	private String symbol;
	private String orderType;
	private BigDecimal orderPrice;
	private String orderId;

	public long getDepositAccountId() {
		return depositAccountId;
	}

	public void setDepositAccountId(long depositAccountId) {
		this.depositAccountId = depositAccountId;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(BigDecimal orderPrice) {
		this.orderPrice = orderPrice;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
