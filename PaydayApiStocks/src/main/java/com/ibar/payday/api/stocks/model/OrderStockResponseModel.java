package com.ibar.payday.api.stocks.model;

public class OrderStockResponseModel {
	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
