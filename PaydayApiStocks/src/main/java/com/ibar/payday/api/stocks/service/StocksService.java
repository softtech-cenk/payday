package com.ibar.payday.api.stocks.service;

import java.util.List;

import com.ibar.payday.api.stocks.dto.OrderDTO;
import com.ibar.payday.api.stocks.dto.StockDTO;

public interface StocksService {
	List<StockDTO> listStocks();
	OrderDTO placeOder(OrderDTO orderDTO);
}
