package com.ibar.payday.api.stocks.dto;

import java.math.BigDecimal;

public class AccountDTO {
	private long depositAccountId;
	private long brokerageAccountId;
	private BigDecimal accountBalance;
	private BigDecimal blockedBalance;

	public long getDepositAccountId() {
		return depositAccountId;
	}

	public void setDepositAccountId(long depositAccountId) {
		this.depositAccountId = depositAccountId;
	}

	public long getBrokerageAccountId() {
		return brokerageAccountId;
	}

	public void setBrokerageAccountId(long brokerageAccountId) {
		this.brokerageAccountId = brokerageAccountId;
	}

	public BigDecimal getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(BigDecimal accountBalance) {
		this.accountBalance = accountBalance;
	}

	public BigDecimal getBlockedBalance() {
		return blockedBalance;
	}

	public void setBlockedBalance(BigDecimal blockedBalance) {
		this.blockedBalance = blockedBalance;
	}

}
