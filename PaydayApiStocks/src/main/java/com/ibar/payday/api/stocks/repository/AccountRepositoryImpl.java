package com.ibar.payday.api.stocks.repository;

import java.math.BigDecimal;

import org.springframework.stereotype.Repository;

import com.ibar.payday.api.stocks.dto.AccountDTO;

@Repository
public class AccountRepositoryImpl implements AccountRepository {

	@Override
	public AccountDTO retrieveAccountIfo(long accountId) {
		AccountDTO accountDTO = new AccountDTO();
		accountDTO.setAccountBalance(new BigDecimal("1000"));
		accountDTO.setBlockedBalance(new BigDecimal("100"));
		accountDTO.setBrokerageAccountId(accountId);
		accountDTO.setDepositAccountId(12345);
		return accountDTO;
	}

	@Override
	public long updateAccountDetails(AccountDTO accountDTO) {
		return accountDTO.getDepositAccountId();
	}

}
