package com.ibar.payday.api.stocks.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ibar.payday.api.stocks.dto.AccountDTO;
import com.ibar.payday.api.stocks.dto.OrderDTO;
import com.ibar.payday.api.stocks.dto.StockDTO;
import com.ibar.payday.api.stocks.model.MockOrderStockRequestModel;
import com.ibar.payday.api.stocks.model.MockOrderStockResponseModel;
import com.ibar.payday.api.stocks.model.MockStockResponseModel;
import com.ibar.payday.api.stocks.repository.AccountRepository;
import com.ibar.payday.api.stocks.repository.MockStockApiClient;
import com.ibar.payday.api.stocks.repository.OrdersRepository;
import com.ibar.payday.api.stocks.service.StocksService;

@Service
public class StocksServiceImpl implements StocksService {

	@Autowired
	MockStockApiClient mockStockApiClient;
	
	@Autowired
	AccountRepository accountRepository; 
	
	@Autowired
	OrdersRepository ordersRepository;
	
	@Override
	public List<StockDTO> listStocks() {
		List<StockDTO> listStocks = new ArrayList<>();
		List<MockStockResponseModel> stocksList = mockStockApiClient.getStocks();
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		for (MockStockResponseModel stock : stocksList) {
			listStocks.add(modelMapper.map(stock, StockDTO.class));
		}
		return listStocks;
	}

	@Override
	public OrderDTO placeOder(OrderDTO orderDTO) {
		// For Buy orders check for available account balance
		AccountDTO accountDTO = accountRepository.retrieveAccountIfo(orderDTO.getDepositAccountId());
		BigDecimal availableBalance = accountDTO.getAccountBalance().subtract(accountDTO.getBlockedBalance());
		if (("BUY".equals(orderDTO.getOrderType())) && (availableBalance.compareTo(orderDTO.getOrderPrice()) < 0)) {
			throw new RuntimeException("Insufficient Account Balance");	
		}
		
		// Update Account blocked amount
		accountDTO.setBlockedBalance(accountDTO.getBlockedBalance().add(orderDTO.getOrderPrice()));
		accountRepository.updateAccountDetails(accountDTO);
		
		// Insert order Info to database
		ordersRepository.insertOrderDetails(orderDTO);
		
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		MockOrderStockRequestModel request = modelMapper.map(orderDTO, MockOrderStockRequestModel.class);
		MockOrderStockResponseModel response = mockStockApiClient.order(request);
		orderDTO.setOrderId(response.getOrderId());
		
		// Update order Info in database with order Id info
		ordersRepository.updateOrderDetails(orderDTO);
		
		return orderDTO;
	}
	
}
