package com.ibar.payday.api.stocks.model;

import java.math.BigDecimal;

public class MockOrderStockRequestModel {
	private long accountId;
	private String symbol;
	private String orderType;
	private BigDecimal orderPrice;

	public long getAccountId() {
		return accountId;
	}

	public void setAccountId(long accountId) {
		this.accountId = accountId;
	}

	public String getSymbol() {
		return symbol;
	}

	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public BigDecimal getOrderPrice() {
		return orderPrice;
	}

	public void setOrderPrice(BigDecimal orderPrice) {
		this.orderPrice = orderPrice;
	}

	@Override
	public String toString() {
		return "OrderStockRequestModel [accountId=" + accountId + ", symbol=" + symbol + ", orderType=" + orderType
				+ ", orderPrice=" + orderPrice + "]";
	}

}
