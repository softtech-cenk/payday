package com.ibar.payday.api.stocks.repository;

import com.ibar.payday.api.stocks.dto.AccountDTO;

public interface AccountRepository {
	AccountDTO retrieveAccountIfo(long accountId);
	long updateAccountDetails(AccountDTO accountDTO);
}
