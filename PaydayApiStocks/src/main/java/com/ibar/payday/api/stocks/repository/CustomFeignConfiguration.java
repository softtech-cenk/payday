package com.ibar.payday.api.stocks.repository;

import org.springframework.context.annotation.Bean;

import feign.Retryer;
import feign.auth.BasicAuthRequestInterceptor;

public class CustomFeignConfiguration {

    @Bean
    public Retryer retryer() {
        return Retryer.NEVER_RETRY;
    }
}