package com.ibar.payday.api.stocks.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.UUID;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.ibar.payday.api.stocks.controllers.StocksController;
import com.ibar.payday.api.stocks.dto.OrderDTO;
import com.ibar.payday.api.stocks.model.MockStockResponseModel;
import com.ibar.payday.api.stocks.service.StocksService;

@WebMvcTest(StocksController.class)
public class StocksControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	StocksService stocksService;
	
	OrderDTO orderDTO;
	
	@BeforeEach
	void setUp() throws Exception {
		orderDTO = new OrderDTO();
		orderDTO.setOrderId(UUID.randomUUID().toString());
	}
	
	@Test
	public void order() throws Exception {
		when(stocksService.placeOder(any())).thenReturn(orderDTO);
		
		mockMvc.perform(
				post("/stocks/order")
				.contentType(MediaType.APPLICATION_JSON)
				.content("{\"accountId\": 3634870, \"symbol\": \"TSLA\", \"orderType\": \"BUY\", \"orderPrice\": 200}"))
		.andExpect(status().isCreated());

	}
	
}
