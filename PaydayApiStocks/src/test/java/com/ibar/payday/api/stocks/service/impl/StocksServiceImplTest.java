package com.ibar.payday.api.stocks.service.impl;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.ibar.payday.api.stocks.model.MockStockResponseModel;
import com.ibar.payday.api.stocks.repository.MockStockApiClient;

@ExtendWith(MockitoExtension.class)
class StocksServiceImplTest {
	
	@InjectMocks
	StocksServiceImpl stocksService = new StocksServiceImpl(); 
	
	@Mock
	MockStockApiClient mockStockService = mock(MockStockApiClient.class);
	
	List<MockStockResponseModel> externalApiResponse = new ArrayList<MockStockResponseModel>();
	
	@BeforeEach
	void setUp() throws Exception {
		MockStockResponseModel stock = new MockStockResponseModel();
		stock.setStockName("Apple");
		stock.setSymbol("APPL");
		stock.setStockPrice(200);
		externalApiResponse.add(stock);
	}
	
	@Test
	void listStocksReturnsStocksWhenExternalServiceIsWorking() {
		when(mockStockService.getStocks()).thenReturn(externalApiResponse);
		assertTrue(stocksService.listStocks().size() > 0, "OK");
	}

}
