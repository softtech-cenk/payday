package com.mock.stock.api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mock.stock.api.dto.MockStockDTO;
import com.mock.stock.api.model.MockStockResponseModel;
import com.mock.stock.api.model.MockOrderStatusResponseModel;
import com.mock.stock.api.model.MockOrderStockRequestModel;
import com.mock.stock.api.model.MockOrderStockResponseModel;
import com.mock.stock.api.service.MockStocksService;

@RestController
@RequestMapping("/mockstocks")
public class MockStockController {

	Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	MockStocksService mockStocksService;
	
	@GetMapping("/healthcheck")
	public String healthCheck() {
		return "Success...";
	}
	
	@GetMapping
	public List<MockStockResponseModel> listStocks() {
		List<MockStockDTO> stocks = mockStocksService.listStocks();
		ModelMapper modelMapper = new ModelMapper();
		modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
		List<MockStockResponseModel> listStocks = new ArrayList<MockStockResponseModel>();
		for (MockStockDTO stock : stocks) {
			listStocks.add(modelMapper.map(stock, MockStockResponseModel.class));
		}
		return listStocks;
	}
	
	@PostMapping("/order")
	public ResponseEntity<MockOrderStockResponseModel> order(@RequestBody MockOrderStockRequestModel orderDeatils) {
		logger.info("new order details: " + orderDeatils.toString());
		MockOrderStockResponseModel responseModel = new MockOrderStockResponseModel();
		responseModel.setOrderId(UUID.randomUUID().toString());
		return ResponseEntity.status(HttpStatus.CREATED).body(responseModel);
	}
	
	@GetMapping("/order/{orderId}")
	public ResponseEntity<MockOrderStatusResponseModel> getOrderStatus(@PathVariable String orderId) {
		logger.info("retrieving order status for: " + orderId);
		MockOrderStatusResponseModel responseModel = new MockOrderStatusResponseModel();
		responseModel.setStatus("COMPLETE");
		return ResponseEntity.status(HttpStatus.OK).body(responseModel);
	}
	
}
