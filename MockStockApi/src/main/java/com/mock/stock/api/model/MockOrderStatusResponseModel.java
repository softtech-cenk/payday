package com.mock.stock.api.model;

public class MockOrderStatusResponseModel {
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
