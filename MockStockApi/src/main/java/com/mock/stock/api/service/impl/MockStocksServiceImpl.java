package com.mock.stock.api.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.stereotype.Service;

import com.mock.stock.api.dto.MockStockDTO;
import com.mock.stock.api.service.MockStocksService;

@Service
public class MockStocksServiceImpl implements MockStocksService {

	@Override
	public List<MockStockDTO> listStocks() {
		List<MockStockDTO> listStocks = new ArrayList<>();
		
		MockStockDTO stock = new MockStockDTO();
		stock.setStockName("Amazon.com Inc.");
		stock.setSymbol("AMZN");
		stock.setStockPrice(3300 + randomWalk());
		listStocks.add(stock);
		
		stock = new MockStockDTO();
		stock.setStockName("Tesla Inc.");
		stock.setSymbol("TSLA");
		stock.setStockPrice(180 + randomWalk());
		listStocks.add(stock);
		
		stock = new MockStockDTO();
		stock.setStockName("Alphabet Inc");
		stock.setSymbol("GOOG");
		stock.setStockPrice(2000 + randomWalk());
		listStocks.add(stock);
		
		return listStocks;
	}
	
	private double randomWalk() {
		Random rand = new Random();
		return ((rand.nextInt(10000) / 100) - 50);
	}

}
