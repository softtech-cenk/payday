package com.mock.stock.api.service;

import java.util.List;

import com.mock.stock.api.dto.MockStockDTO;

public interface MockStocksService {
	List<MockStockDTO> listStocks();
}
