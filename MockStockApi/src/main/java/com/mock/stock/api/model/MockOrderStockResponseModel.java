package com.mock.stock.api.model;

public class MockOrderStockResponseModel {
	private String orderId;

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

}
